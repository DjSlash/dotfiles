syntax on
set background=dark
colorscheme industry
if has("autocmd")
	  filetype plugin indent on
endif
set showcmd
set showmatch
set number 
set laststatus=2 
set listchars=tab:▸\ ,eol:¬
set ruler
set modeline
let g:Tex_AutoFolding = 0

packadd! gnupg
