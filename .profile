# I symlink this file as ~/.profile and ~/.zprofile,
# so that they work for either bash or zsh.

# set a more restrictive umask
umask 027

# Add my local bin when it exists
[ -d $HOME/.local/bin ] && export PATH="$HOME/.local/bin:$PATH"

# Language specific settings
export LANG="en_US.utf8"
export LC_NUMERIC="nl_NL.utf8"
export LC_TIME="nl_NL.utf8"
export LC_COLLATE="nl_NL.utf8"
export LC_MONETARY="nl_NL.utf8"
export LC_PAPER="nl_NL.utf8"
export LC_NAME="nl_NL.utf8"
export LC_ADDRESS="nl_NL.utf8"
export LC_TELEPHONE="nl_NL.utf8"
export LC_MEASUREMENT="nl_NL.utf8"
export LC_IDENTIFICATION="nl_NL.utf8"

# XDG base directories
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# I use my hostname quite often
export HOSTNAME=$(hostname -s)
