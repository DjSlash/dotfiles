# dotfiles of DjSlash

Once you start with your dotfiles, you never finish.


## Desktop environment

I used to use **XFCE**, but have switched to **i3wm**.

Most common applications that I use often:
 * urxvt
 * Firefox
 * Thunderbird
 * signal-desktop
 * digiKam

## Shell
My standard shell is **zsh**, with the addition of oh-my-zsh. But I keep my bashrc
around for those places where I don't have zsh available.

For places where I login and have multiple things running, I use **screen**. Got
used by it long time ago and sticked with it.

# Credits
lockscreen.png is "[in tux we trust](https://gitlab.com/jstpcs/lnxpcs/blob/master/non-distro/in-tux-we-trust.png)"
